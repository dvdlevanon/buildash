const JobsConfigBase = require('jobs/jobs-config-base');

class JobsConfigWrapper {
	static wrap(jobsConfigScriptContent) {
		var configClass = `
(class JobsConfig extends JobsConfigBase {
	` + jobsConfigScriptContent + `
})
`;
		var JobsConfig = eval(configClass);
		
		return new JobsConfig();
	}
}

module.exports = JobsConfigWrapper;
