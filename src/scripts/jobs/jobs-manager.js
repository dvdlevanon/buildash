class JobsManager {
	constructor(jobsConfig, jobs) {
		this.jobsConfig = jobsConfig;
		this.jobs = jobs;
	}
}

module.exports = JobsManager;
