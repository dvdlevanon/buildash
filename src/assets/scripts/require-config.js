if (typeof requirejs !== 'undefined') {
	requirejs.config({
		baseUrl: "/scripts",
		paths: {
			"jquery": "libs/jquery/dist/jquery",
			"jquery-ui": "libs/jquery-ui-dist/jquery-ui",
			"lodash": "libs/lodash/lodash",
			"async": "libs/async/dist/async"
		}
	});

	define(function(require) { 
		require("/scripts/dashboard.js");
	});
}
