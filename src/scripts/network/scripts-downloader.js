const $ = require("jquery");

class ScriptDownloader {
	download(scriptUrl, callback) {
		callback(null, `
defineJobs() {
	return {
		"SaaS":{
			"Server":{
				"Backend":{},
				"BackendConfig":{},
				"Frontend":{}
			},
			"DeployServer":{}
		},
		"Shark":{
			"SharkAll":{
				"SharkWar": {
					"Backend":{},
					"BackendConfig":{},
					"Frontend":{},
					"War":{},
					"StandaloneWar":{}
				},
				"SharkTomcat": {
					"Backend":{},
					"BackendConfig":{},
					"Frontend":{},
					"War":{},
					"StandaloneWar":{},
					"Tomcat":{},
					"StandaloneTomcat":{}
				},
				"SharkDocker": {
					"Backend":{},
					"BackendConfig":{},
					"Frontend":{},
					"Tomcat":{},
					"Dynalite":{},
					"TakipiStorage":{},
					"OnpremDocker":{}
				}
			},
			"DeployShark":{}
		},
		"Hybrid":{
			"Hybrid":{
				"TakipiStorage":{}
			},
			"DeployHybrid":{}
		}
	};
}
`);
	}
	
	downloadMany(scriptNameUrlMap, callback) {
		var result = {};
		
		for (var scriptName in scriptNameUrlMap) {
			result[scriptName] = "";
		}
		
		callback(null, result);
	}
}

module.exports = ScriptDownloader;
