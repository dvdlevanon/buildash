const $ = require("jquery");
const jqueryUI = require("jquery-ui");

class DashboardInitializer {
	initialize() {
		$(".jobs_holder .jobs").accordion({
			heightStyle: "fill"
		});
		
		return true;
	}
}

module.exports = DashboardInitializer;
