
class JobsConfigBase {
	constructor() {
		this.jobNameTree = this.defineJobs();
	}
	
	collectJobsNames() {
		var result = {};
		this.collectJobNames(this.jobNameTree, result);
		return result;
	}
	
	collectJobNames(jobNode, result) {
		if (!jobNode) {
			return;
		}
		
		for (var childName in jobNode) {
			var childNode = jobNode[childName];
			
			result[childName] = null;
			this.collectJobNames(childNode, result);
		}
	}
	
	collectJobsUrls() {
		var jobNames = this.collectJobsNames(this.jobNameTree);
		var result = {};
		
		for (var jobName in jobNames) {
			result[jobName] = "buildash/jobs/" + jobName + "/config.js";
		}
		
		return result;
	}
}

module.exports = JobsConfigBase;
