const JobBase = require('jobs/job-base');

class JobWrapper {
	static wrap(jobName, jobScriptContent) {
		var configClass = `
(class ` + jobName + `JobConfig extends JobBase {
	constructor() {
		super();
		this.name = "` + jobName + `";
	}
	
	` + jobScriptContent + `
})
`;
		var JobConfig = eval(configClass);
		
		return new JobConfig();
	}
}

module.exports = JobWrapper;
