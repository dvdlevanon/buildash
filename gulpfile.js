const gulp = require('gulp');
const browserify = require('gulp-browserify');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const image = require('gulp-image');
const inject = require('gulp-inject');
const serve = require('gulp-serve');
const environments = require('gulp-environments');
const commonToAmd = require('gulp-common-to-amd');
const debugCollectedFiles = require('gulp-debug');
const symlink = require('gulp-symlink');
const browserResolve = require('browser-resolve');
const gulpSequence = require('gulp-sequence')
 
const development = environments.development;
const production = environments.production;

function browserifyResolver() {
	return function(id, opts, cb) {
		if (opts.paths.indexOf('src/scripts') == -1)
		{
			opts.paths.push('src/scripts');
		}
		
		return browserResolve(id, opts, cb);
	}
}

gulp.task('scripts', function() {
	var sources = production() ?
		'src/scripts/dashboard.js' :
		'src/scripts/**/*.js';
	
	if (development())
	{
		gulp.src('node_modules')
			.pipe(debugCollectedFiles({ title: 'libs' }))
			.pipe(symlink.absolute("tmp/scripts/libs", { 
				force: true 
			} ));
	}
	
	return gulp.src(sources)
		.pipe(debugCollectedFiles({ title: 'browserify' }))
		.pipe(production(browserify( { 
			resolve: browserifyResolver(), 
			debug: true 
		})))
		.pipe(development(commonToAmd()))
		.pipe(gulp.dest('tmp/scripts'));
	
});

gulp.task('styles', function() {
	gulp.src('./src/styles/dashboard.scss')
		.pipe(debugCollectedFiles({ title: 'sass' }))
		.pipe(sass())
		.pipe(gulp.dest('tmp/css'));
	
	var providedCss = [
		'node_modules/jquery-ui-dist/jquery-ui.css'
	];
	
	return gulp.src(providedCss)
		.pipe(debugCollectedFiles({ title: 'provided-css' }))
		.pipe(gulp.dest('tmp/css'));
});

gulp.task('images', function() {
	var providedImages = [
		'node_modules/jquery-ui-dist/images/**'
	];
	
	gulp.src(providedImages)
		.pipe(debugCollectedFiles({ title: 'provided-images' }))
		.pipe(gulp.dest('tmp/css/images'));
	
	return gulp.src('./src/images/*')
		.pipe(debugCollectedFiles({ title: 'images' }))
		.pipe(production(image()))
		.pipe(gulp.dest('tmp/images'));
});

gulp.task('views', function() {
	var sources = gulp.src(['tmp/scripts/*.js', 'tmp/css/*.css'], {read: false});
	
	sources
		.pipe(debugCollectedFiles( { title: 'inject-sources' } ));
	
	return gulp.src('src/views/dashboard.pug')
		.pipe(debugCollectedFiles({ title: 'pug' }))
		.pipe(inject(sources, { 
			ignorePath: 'tmp' 
		}))
		.pipe(pug({
			pretty: true,
			locals: {
				development: development(),
				production: production()
			}
		}))
		.pipe(gulp.dest('tmp/views'));
});

gulp.task('assets', function() {
	return gulp.src('./src/assets/**')
		.pipe(debugCollectedFiles({ title: 'assets' }))
		.pipe(gulp.dest('tmp'));
});

gulp.task('serve', serve('tmp'));

gulp.task('default', gulpSequence('assets', 'scripts', 'styles', 'images', 'views'));
