const Async = require('async');
const JobsManager = require('jobs/jobs-manager');
const JobsConfigWrapper = require('jobs/jobs-config-wrapper');
const JobWrapper = require('jobs/job-wrapper');

class JobsManagerLoader {
	static load(scriptsDownloader, jobsConfigUrl, callback) {
		var owner = this;
		
		Async.waterfall([
			function loadJobsConfig(callback) {
				owner.loadJobsConfig(scriptsDownloader, jobsConfigUrl, function done(err, jobsConfig) {
					callback(err, jobsConfig);
				});
			},
			function loadJobs(jobsConfig, callback) {
				owner.loadJobs(scriptsDownloader, jobsConfig, function done(err, jobs) {
					callback(err, jobsConfig, jobs);
				});
			},
			function createJobsManager(jobsConfig, jobs) {
				callback(null, new JobsManager(jobsConfig, jobs));
			}
		],
		function done(err, jobsManager) {
			callback(err, jobsManager);
		});
	};
	
	static loadJobsConfig(scriptsDownloader, jobsConfigUrl, callback) {
		var owner = this;
		
		Async.waterfall([
			function loadScript(callback) {
				
				scriptsDownloader.download(jobsConfigUrl, function done(err, jobConfigScriptContent) {
					callback(err, jobConfigScriptContent);
				});
			},
			function createJobsConfig(jobConfigScriptContent, callback) {
				
				var jobsConfig = JobsConfigWrapper.wrap(jobConfigScriptContent);
				
				if (!jobsConfig) {
					return callback("Error parsing " + jobsConfigUrl);
				}
				
				callback(null, jobsConfig);
			}
		],
		function done(err, jobsConfig) {
			callback(err, jobsConfig);
		});
	};
	
	static loadJobs(scriptsDownloader, jobsConfig, callback) {
		var jobNameUrlMap = jobsConfig.collectJobsUrls();
		
		Async.waterfall([
			function loadJobs(callback) {
				
				scriptsDownloader.downloadMany(jobNameUrlMap, function done(err, jobScriptContentMap) {
					callback(err, jobScriptContentMap);
				});
			},
			function createJobs(jobScriptContentMap, callback) {
				
				var jobs = {};
				
				for (var jobName in jobScriptContentMap) {
					jobs[jobName] = JobWrapper.wrap(jobName, jobScriptContentMap[jobName]);
				}
				
				callback(null, jobs);
			}
		],
		function done(err, jobs) {
			callback(err, jobs);
		});
	};
}

module.exports = JobsManagerLoader;
