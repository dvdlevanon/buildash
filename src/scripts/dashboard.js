const $ = require("jquery");
const jqueryUI = require("jquery-ui");
const DashboardInitializer = require("dashboard-initializer");
const ScriptsDownloader = require("network/scripts-downloader");
const JobsManagerLoader = require("jobs/jobs-manager-loader");
const JobsManager = require("jobs/jobs-manager");

$(document).ready(function() {
	var dashboardInitializer = new DashboardInitializer();
	
	if (!dashboardInitializer.initialize()) {
		console.log("Error initializing dashboard components");
	}
	
	var scriptsDownloader = new ScriptsDownloader();
	var jobsManager = null;
	
	JobsManagerLoader.load(scriptsDownloader, "buildash-config/jobs-config.js", 
		function done(err, loadedJobsManager) {
			jobsManager = loadedJobsManager;
			console.log(jobsManager);
		}
	);
});
